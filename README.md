# Tindin Front-End Test

## Tecnologias Utilizadas

- Bootstrap;
- Typescript;
- Angular;

## Descrição

Criar telas responsivas de Login, Cadastro e Catálogo de Aulas, além de consumir uma API para autenticação do usuário.

## Execução

- Clone o repositório;

- Execute o comando `npm install` no terminal para instalar as dependências;

- Execute o comando `ng serve` no terminal para executar o projeto;

Acesse o link http://localhost:4200, se esta porta estiver em uso, verifique a porta a ser utilizada no seu terminal.
