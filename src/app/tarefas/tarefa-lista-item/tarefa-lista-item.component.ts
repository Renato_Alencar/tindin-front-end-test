import { Component, OnInit, Input } from '@angular/core';
import { Tarefa } from '../compartilhada/tarefa';
import { TarefaService } from '../compartilhada/tarefa.service';

@Component({
  selector: 'app-tarefa-lista-item',
  templateUrl: './tarefa-lista-item.component.html',
  styleUrls: ['./tarefa-lista-item.component.css'],
})
export class TarefaListaItemComponent implements OnInit {
  @Input() aula: Tarefa = new Tarefa();

  constructor(private tarefaService: TarefaService) {}

  ngOnInit(): void {}

  date: string = this.tarefaService.getCurrentDate();

  removeAula(aula: Tarefa): void {
    this.tarefaService.deleteAula(aula.id);
  }
}
