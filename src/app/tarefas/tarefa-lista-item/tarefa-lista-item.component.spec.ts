import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TarefaListaItemComponent } from './tarefa-lista-item.component';

describe('TarefaListaItemComponent', () => {
  let component: TarefaListaItemComponent;
  let fixture: ComponentFixture<TarefaListaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TarefaListaItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TarefaListaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
