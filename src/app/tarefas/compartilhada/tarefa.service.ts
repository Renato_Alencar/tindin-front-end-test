import { Injectable } from '@angular/core';

import { Tarefa } from './tarefa';

@Injectable({
  providedIn: 'root',
})
export class TarefaService {
  aulas: Tarefa[] = [];

  constructor() {}

  setLocalStorage(): void {
    window.localStorage.setItem('lista-aulas', JSON.stringify(this.aulas));
  }

  getAllAulas(): Tarefa[] {
    const listAulas = window.localStorage.getItem('lista-aulas');
    if (listAulas) {
      this.aulas = JSON.parse(listAulas);
    }
    return this.aulas;
  }

  getIdAula(id: number): Tarefa | any {
    return this.aulas.find((aula) => aula.id == id);
  }

  saveAula(aula: Tarefa): void {
    if (aula.id) {
      let arrayAulas = this.getIdAula(aula.id);
      arrayAulas.title = aula.title;
    } else {
      let lastId = 0;
      if (this.aulas.length > 0) {
        lastId = this.aulas[this.aulas.length - 1].id;
      }
      aula.id = lastId + 1;
      this.aulas.push(aula);
    }
    this.setLocalStorage();
  }

  getCurrentDate(): string {
    let today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getDate() + 1).padStart(2, '0');
    const yyyy = String(today.getFullYear());

    return `${dd}/${mm}/${yyyy}`;
  }

  deleteAula(id: number): void {
    const indexAula = this.aulas.findIndex((aula) => aula.id === id);
    this.aulas.splice(indexAula, 1);
    this.setLocalStorage();
  }
}
