import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tarefa } from '../compartilhada/tarefa';
import { TarefaService } from '../compartilhada/tarefa.service';

@Component({
  selector: 'app-tarefa-formulario',
  templateUrl: './tarefa-formulario.component.html',
  styleUrls: ['./tarefa-formulario.component.css'],
})
export class TarefaFormularioComponent implements OnInit {
  aula: Tarefa = new Tarefa();
  titleForm: string = 'Nova Tarefa';

  constructor(
    private activedRoute: ActivatedRoute,
    private router: Router,
    private tarefaService: TarefaService
  ) {}

  ngOnInit(): void {
    const id = this.activedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.aula = this.tarefaService.getIdAula(parseInt(id));
      this.titleForm = 'Alterando Tarefa';
    }
  }

  onSubmit(): void {
    this.tarefaService.saveAula(this.aula);
    this.tarefaService.getCurrentDate();
    this.router.navigate(['/aulas']);
  }
}
