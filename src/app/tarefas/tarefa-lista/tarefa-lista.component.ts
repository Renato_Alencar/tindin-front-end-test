import { Component, OnInit } from '@angular/core';
import { Tarefa } from '../compartilhada/tarefa';
import { TarefaService } from '../compartilhada/tarefa.service';

@Component({
  selector: 'app-tarefa-lista',
  templateUrl: './tarefa-lista.component.html',
  styleUrls: ['./tarefa-lista.component.css'],
})
export class TarefaListaComponent implements OnInit {
  aulas: Tarefa[] = [];

  constructor(private tarefaService: TarefaService) {}

  ngOnInit(): void {
    this.aulas = this.tarefaService.getAllAulas();
  }
}
