import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { interval } from 'rxjs';

export interface Login {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private router: Router) {}

  login: Login = {
    email: '',
    password: '',
  };

  emailDefault: string = 'tester@tindin.com.br';
  passwordDefault: string = 'tester123';
  message: string = '';

  entrar() {
    if (
      this.login.email === this.emailDefault &&
      this.login.password === this.passwordDefault
    ) {
      this.router.navigateByUrl('/aulas');
    } else {
      var counter = 0;
      var interval = setInterval((): void => {
        counter++;
        if (counter < 5) this.message = 'E-mail e/ou senha inválidos!';
        else {
          this.message = '';
          clearInterval(interval);
          return;
        }
      }, 1000);
    }
  }
}
