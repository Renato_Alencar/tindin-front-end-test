import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TarefaFormularioComponent } from './tarefas/tarefa-formulario/tarefa-formulario.component';
import { TarefaListaComponent } from './tarefas/tarefa-lista/tarefa-lista.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'aulas', component: TarefaListaComponent },
  { path: 'cadastro', component: TarefaFormularioComponent },
  { path: 'edit/:id', component: TarefaFormularioComponent },
  { path: '', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
