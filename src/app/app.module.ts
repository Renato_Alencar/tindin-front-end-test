import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TarefaListaComponent } from './tarefas/tarefa-lista/tarefa-lista.component';
import { TarefaListaItemComponent } from './tarefas/tarefa-lista-item/tarefa-lista-item.component';
import { TarefaFormularioComponent } from './tarefas/tarefa-formulario/tarefa-formulario.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    TarefaListaComponent,
    TarefaListaItemComponent,
    TarefaFormularioComponent,
    LoginComponent,
  ],
  imports: [BrowserModule, FormsModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
